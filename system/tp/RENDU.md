Camille Dubourgeois 

#Rendu **TP - System**


Questions :
- Un flake nix est un répertoire avec un flake.nix et flake.lock à la racine qui génère des expressions Nix que d'autres peuvent utiliser pour faire des choses comme créer des packages, exécuter des programmes, utiliser des environnements de développement ou mettre en place des systèmes NixOS. 

*TP SYSTEM**
- Fichier flake.nix
```bash
# flake est une configuration pour créer un environnement de développement Nix avec des outils spécifiques. 
{
  description = "Open source system tp"; #Contexte du flake d'utilisation
  inputs = { #Entrées nécessaire a la construction du flake

    nixpkgs.url = "github:NixOS/nixpkgs/nix"; #Dépot github contenant la collection des paquets Nix de NixOS

    flake-utils.url = "github:numtide/flake-utils"; #Flake utilisé pour la configuration de nix
  };

  outputs = #fonction à un argument qui prend un ensemble d'attributs de toutes les entrées réalisées et produit un autre ensemble d'attributs.

    { self #ce flak
    , nixpkgs #sortie nixpkgs
    , flake-utils #sortie flake-utils
    , ... #autres sorties
    }:

    flake-utils.lib.eachDefaultSystem (system: #eachDefaultSystem est une fonction de flake-utils qui itère sur les différents systèmes par défaut pris en charge par Nix

    let
      pkgs = import nixpkgs { #import de nixpkgs 
        inherit system;
      };
      tpPackages = [ #liste de paquets Nix qui vont être utilisés
        pkgs.go-task
        pkgs.podman
        pkgs.rustfmt
        pkgs.trivy
        pkgs.hadolint
      ];
    in
    {
      devShells.default = # définition de la sortie, elle permettra a mkShell de créer un environnement de dev Nix contenant certaines dépendances, dont celle défini ci-dessous ( nativeBuildInputs )
        pkgs.mkShell
          {
            nativeBuildInputs = [
                
              pkgs.dapr-cli
              pkgs.stdenv
              pkgs.go
              pkgs.glibc.static
            ] ++ tpPackages;
          };
    });
}
```
- Nixpkgs est l’ensemble des milliers de packages pour le gestionnaire de packages Nix. Il implémente également NixOS.

- Nixpkgs est le référentiel central de NixOS contenant des milliers de définitions de paquets et de configurations pour les logiciels. C'est essentiel pour la création d'environnements logiciels reproductibles et personnalisables.

- Car Nix utilise son propore language pour la définition des flaks ( et pas que...)

- Mot clé Dockefile CMD : 
    CMD défini une commande qui pourra être lancé lors du lancement du conteneur après compilation, mais celle si peut être override lors d'un docker run.

- Mot clé Dockefile ENTRYPOINT :
    Le ENTRYPOINT sert a définir une commande qui sera lancée sans excpetion lors du démarrage du docker, il n'est pas possible de l'override.               

###Dockerfile revisité :
```docker
FROM docker.io/debian:latest

RUN \
  apk add cargo bash && cargo install tealdeer && tldr --update && 
  curl -sfL ... | sh -s -- -b /usr/local/bin && trivy filesystem --exit-code 1 --no-progress / && trivy filesystem --exit-code 1 --no-progress /

CMD []

CMD ["tldr"]
```

- Les cgroups permettent de limiter la vision des processus sur les control-groups

- Les namespaces font pareil mais rajoute aussi la limite de vision des processus sur les ressources


- Containerd est un composant de l'écosystème de conteneurisation qui permet la gestion des containers.

- Les syscalls utilisés : clone, chroot, cgroup, mount, unmount, setns, pivot_root, sendfile, ioctl.

- Un container alpine est plus petit par la taille, il comprend moins de logiciel préinstallé ce qui lui permet de lancer le conteneur plus rapidement, c'est aussi plus sécurisé.


-musl est une alternative a glibc, il est utilisé car il est plus légé, plus performant, compatible avec POSIX, il est plus sécurisé et il réduit la taille de l'image.
 