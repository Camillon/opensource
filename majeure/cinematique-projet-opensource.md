# Les projets Open source

## Chronique d'un projet libre ou open-source

### Naissance

* Un hack, une expérience, une recherche, tentative humble de réaliser quelques chose. (linux)
* Un besoin née d'une réflexion et/ou de l'absence de solution libre ou ouverte (nagios/netbox)
* Livraison d'un projet à la communautée (probablement associé à la création d'une fondation)
* ajout sur un projet existant

### Gains pour la comunautés

* Une solution disponible à moindre coût
* Nouvel Apport fonctionnel ouvert : implémentation métié adaptable offrant un framework (nagios, docker)
* Base de recherche et source d'inspiration.

### Apport de la communuté

* Utilisation/Evaluation/tests
  * retours d'utilisation et propositions
  * déclaration d'anomalie
* Implication dans le projet :
  * mediatisation : publication externe,
  * relecture/traduction de doc
* participation au projet :
  * participation active dans le user group
  * proposer une pull request de code
* gestion d'un projet connexe
  * plugins / add on
  * communauté d'utilisateur et partage de ressources

### L'essort

* Le projet trouve ses clients et utilisateurs
* Une structure s'associe au projet et stabilise son financement.

#### Le Financement

* Dons des utilisateurs
* Sponsort de Fondations : Objectif de la fondation
* Mecenat d'Entreprises : Quid de la contrepartie
* Vente de services/solution associés : L'Autonomie ou la galère

### Le déclin ?

* Bascule du coté fermé.
* Les Forks à destination rentable.
* L'Obsolescence lié à l'émergence d'une nouvelle tendence (ou Rachat par IBM...)

## Les fondations

Voir l'[histoire d'Unix](https://fr.wikipedia.org/wiki/Unix)

### Free software fondation FSF

créé par Monsieur Richard Stallman pour financer le projet GNU et promouvoir le logiciel libre

La FSF est une organisation américaine à but non lucratif fondée par Richard Stallman le 4 octobre 1985, dont la mission est la promotion du logiciel libre et la défense des utilisateurs. La FSF aide également au financement du projet GNU depuis l'origine. Son nom est associé au mouvement du logiciel libre. ([wikipédia](https://fr.wikipedia.org/wiki/Free_Software_Foundation))

#### Les projets de la FSF

Le projet GNU a une dimension social ethique et politique : L'objectif est d'avoir une version **libre** des logiciels courament utilisés

* GNU
  * GNU/Linux et GNU/hurd
  * Debian
  * GNOME
* system exploitation pour téléphone : <https://replicant.us/>
* manipulation d'images : The Gimp
* video : Blender
* Collection SAASS Service As A Software Substitute : <https://directory.fsf.org/wiki/Collection:SaaSS>
* format de fichier : <https://directory.fsf.org/wiki/Collection:File_format>

> [projets prioritaires de la FSF](https://www.fsf.org/campaigns/priority-projects/)

### L'open group

Issue de la fusion entre :

* l'**Open Software Foundation**, organisation fondée en 1988 (Apollo, Bull, DEC, HP, IBM, Nixdorf) pour créé un standard ouvert pour le système d'exploitation Unix ([wikipedia](https://fr.wikipedia.org/wiki/Open_Software_Foundation))
* et l' **X/Open** consortium européen (Bull, ICL, Siemens, Olivetti et Nixdorf) voulant definir une spécification commune aux systèmes d'exploitation dérivés d'UNIX

#### Les projet de l'open group

* Single Unix specification et UNIX98
* POSIX
* CDE (common Desktop environrment)
* LDAP
* Linux Standard Base
* X.org

### La CNCF

Cloud Native Computing Fondation : projet de la linux fondation autour de la conteneurisation. Projet créé en 2015 en partenariant entre la Linux fondation et Google qui livrais alors Kubernetes v1.0 comme première technologie.

L'Objectif de cette fondation est de faire progresser les technologies liée à la conteneurisation.

C'est une communautée des 450 acteurs du cloud (dont les acteurs majeurs tel que Google, aws, ~~RedHat~~ IBM, Cisco, Intel, VMware ...)

La CNCF recences les projets "cloud native" par niveau de maturité (Graduated, Incubating, Sandbox) et en fait la promotion.

![landscape-open-source](../images/OS-CNCF-Landscape.png)

je vous encourage a consulter la [carte](https://landscape.cncf.io/?license=open-source&project=graduated,incubating&zoom=80) interactive.
